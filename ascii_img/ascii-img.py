
penguin = """
   _~_
  (o o)
 /  V  \\
/(  _  )\\
  ^^ ^^
"""

cow = """
\   ,__,
 \  (oo)____
    (__)    )\\
        ||--|| *
"""

daemon = """
    \         ,        ,
     \       /(        )`
      \      \ \___   / |
             /- _  `-/  '
            (/\/ \ \   /\\
            / /   | `    \\
            O O   ) /    |
            `-^--'`<     '
           (_.)  _  )   /
            `.___/`    /
              `-----' /
 <----.     __ / __   \\
 <----|====O)))==) \) /====
 <----'    `--' `.__,' \\
              |        |
               \       /
         ______( (_  / \______
       ,'  ,-----'   |        \\
       `--{__________)        \/
"""

dragon = """
                       \                    ^    /^
                        \                  / \  // \\
                         \   |\___/|      /   \//  .\\
                          \  /O  O  \__  /    //  | \ \           *----*
                            /     /  \/_/    //   |  \  \          \   |
                            @___@`    \/_   //    |   \   \         \/\ \\
                           0/0/|       \/_ //     |    \    \         \  \\
                       0/0/0/0/|        \///      |     \     \       |  |
                    0/0/0/0/0/_|_ /   (  //       |      \     _\     |  /
                 0/0/0/0/0/0/`/,_ _ _/  ) ; -.    |    _ _\.-~       /   /
                             ,-}        _      *-.|.-~-.           .~    ~
            \     \__/        `/\      /                 ~-. _ .-~      /
             \____(oo)           *.   }            {                   /
             (    (--)          .----~-.\        \-`                 .~
             //__\\  \__ Mooo!   ///.----..<        \             _ -~
            //    \\               ///-._ _ _ _ _ _ _{^ - - - - ~
"""


images_collection = {
    "cow": cow,
    "daemon": daemon, 
    "dragon": dragon, 
    "penguin": penguin, 
}


def print_ascii_image(image, count=1, max_width=80):
    if not isinstance(max_width, int) or max_width <= 0:
        raise TypeError("max width must be positive integer")
    if not isinstance(count, int) or count <= 0:
        raise TypeError("count of images must be positive integer")
    image_width = max(len(i) for i in image.splitlines()) + (1 if count > 1 else 0)
    if max_width < image_width:
        raise RuntimeError("the width is very small for draw this image")
    image_rows = [i.ljust(image_width-1) for i in image[1:].splitlines()]
    page_size = int(max_width / image_width)
    remained = count
    while remained:
        if remained != count:
            print()
        page = page_size if remained > page_size else remained
        for line in image_rows:
            print("{}".format((line+" ")*page))
        remained -= page


if __name__ == "__main__":
    import optparse
    import os
    import sys

    try:
        TTY_HEIGHT, TTY_WIDTH = [int(i) for i in os.popen('stty size', 'r').read().split()]
    except Exception:
        TTY_WIDTH = 80

    default_image = "penguin"

    parser = optparse.OptionParser()
    parser.add_option(
        "-l", dest="list", action="store_true", default=False, help="show avaiable images"
    )
    parser.add_option("-v", dest="verbose", action="store_true", default=False, help="verbose")
    parser.add_option(
        "-i", dest="image",
        default=default_image, help="image type, default is '{}'".format(default_image)
    )
    parser.add_option("-n", dest="numbers", default=1, help="number of copies to output")
    parser.add_option("-w", dest="width", default=TTY_WIDTH, help="terminal width")

    (options, args) = parser.parse_args()

    if options.list:
        if options.verbose:
            for name, image in images_collection.items():
                print("{}:\n".format(name))
                print_ascii_image(image, count=1, max_width=100)
                print()
        else:
            print("Avaiable images: {}".format(' '.join(k for k in images_collection)))
        sys.exit(0)
    try:
        print_ascii_image(
            images_collection[options.image],
            count=int(options.numbers),
            max_width=int(options.width)
        )
    except ValueError:
        parser.error("use positive integers for numbers")
    except Exception as e:
        parser.error(str(e))
