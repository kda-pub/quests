## Init

```bash
python3.6 -m venv .venv
source .venv/bin/activate
CRYPTOGRAPHY_DONT_BUILD_RUST=1 pip install -r requirements.txt
```

## Strart ssh server

```bash
./stack.sh
```

## Run client

```bash
source .venv/bin/activate
python client.py
```
