import contextlib
import warnings

warnings.filterwarnings(action='ignore',module='.*paramiko.*')

import paramiko


class SSHClient(paramiko.client.SSHClient):

    def run_command(self, cmd: str) -> str:
        with self.get_transport().open_session() as channel:
            channel.get_pty()
            channel.exec_command(cmd)
            buffer, data = (b'', channel.recv(1024))
            while data:
                buffer += data
                data = channel.recv(1024)
            return buffer.decode().strip("\n")


@contextlib.contextmanager
def ssh_connection(*args, **kwargs) -> paramiko.client.SSHClient:
    kwargs.update({"banner_timeout": 10, "allow_agent": False, "look_for_keys": False})
    with SSHClient() as client:
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(*args, **kwargs)
        yield client


if __name__ == '__main__':
    creds = {
        'hostname': '127.0.0.1',
        'port': 2222,
        'username': 'test_user',
        'password': 's12345678',
        'timeout': 15
    }
    with ssh_connection(**creds) as ssh_connection:
        commands = (
            "uname -a",
            "ping -c 5 127.0.0.1",
            "du -h -m 0 /var",
        )
        for c in commands:
            print("\n{}".format(c))
            output = ssh_connection.run_command(c)
            print("\n{}".format(output))
