import argparse
import dataclasses
import datetime
import json
import pathlib
from typing import Iterator, Union

TIMESTAMP_FORMAT = "%Y-%m-%d %H:%M:%S"


@dataclasses.dataclass
class Log:
    index: int  # индекс генератора строк лога
    line: Union[bytes, None]  # строка с сообщением из файла лога


def iter_file_lines(file_path: pathlib.Path) -> Iterator[bytes]:
    if not file_path.exists():
        raise FileExistsError("Logfile '%s' not exist".format(file_path))
    with open(file_path, "rb") as f:
        line = f.readline()
        while line:
            yield line
            line = f.readline()


def merge_sorted(*streams: Iterator[bytes]) -> Iterator[bytes]:

    def timestamp(message):
        msg = json.loads(message)
        str2dt = datetime.datetime.strptime
        return str2dt(msg.get("timestamp"), TIMESTAMP_FORMAT).timestamp()

    def _key(item):
        return timestamp(item.line)

    indexes = list(range(len(streams)))
    registry = []
    if len(indexes) > 1:
        # сюда будем затягивать сообщения
        registry = [Log(index=i, line=None) for i in indexes]
    while indexes:
        if len(indexes) > 1:
            # тут мы будем упорядочивать сообщения
            try:
                for i in range(len(registry)):
                    if registry[i].line is None:
                        registry[i].line = next(streams[registry[i].index])
            except StopIteration:
                # сообщения из файла закончились -> убираем его из обработки
                indexes.remove(registry[i].index)
                registry.remove(registry[i])
                continue
            # сортируем и отдаем первый
            registry = sorted(registry, key=_key)
            yield registry[0].line
            # очистим значение строки которую отдали
            registry[0].line = None
        else:
            # не забываем строки, которые уже извлечены из файла и могли
            # остаться при сортировке
            yield from (i.line for i in registry if i.line is not None)
            # тут мы просто прокручиваем до конца оставшийся файл, сортировать
            # не нужно
            yield from (i for i in streams[indexes[0]])
            indexes.remove(indexes[0])


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description='Tool for merge two logs')

    parser.add_argument(
        'logs',
        metavar='<path/to/logN>',
        nargs='+',
        type=str,
        help='paths of logs files for merge',
    )

    parser.add_argument(
        '-o', '--output',
        type=str,
        metavar='<path/to/merged/log>',
        required=True,
        help='merged log file',
        dest='merged_log',
    )

    return parser.parse_args()


def main() -> None:
    args = parse_args()
    with open(pathlib.Path(args.merged_log), "wb") as f:
        streams = [iter_file_lines(pathlib.Path(i)) for i in args.logs]
        for message in merge_sorted(*streams):
            f.write(message)


if __name__ == '__main__':
    main()
