import array
import functools
import time


def timeit(f):
    @functools.wraps(f)
    def wrap(*args, **kw):
        t1 = time.time()
        result = f(*args, **kw)
        t2 = time.time()
        print('func:%r took: %2.15f sec' % (f.__name__, t2 - t1))
        return result
    return wrap


def isexist(mass: array.array, value) -> bool:
    first = 0
    last = len(mass) - 1
    found = False

    while first <= last and not found:
        midpoint = (first + last) // 2
        if mass[midpoint] == value:
            found = True
        else:
            if value < mass[midpoint]:
                last = midpoint - 1
            else:
                first = midpoint + 1
    return found


def mergesort(mass: array.array) -> None:
    if len(mass) > 1:
        r = len(mass)//2
        L = mass[:r]
        M = mass[r:]

        mergesort(L)
        mergesort(M)

        i = j = k = 0

        while i < len(L) and j < len(M):
            if L[i] < M[j]:
                mass[k] = L[i]
                i += 1
            else:
                mass[k] = M[j]
                j += 1
            k += 1

        while i < len(L):
            mass[k] = L[i]
            i += 1
            k += 1

        while j < len(M):
            mass[k] = M[j]
            j += 1
            k += 1


if __name__ == '__main__':
    import random
    import sys

    def human_size(bytes, units=[' bytes','KB','MB','GB','TB', 'PB', 'EB']):
        """ Returns a human readable string representation of bytes """
        return str(bytes) + units[0] if bytes < 1024 else human_size(bytes>>10, units[1:])


    @timeit
    def search0(a, v):
        return isexist(a, v)

    @timeit
    def search1(a, v):
        return v in a


    UNIQUE_SET = set()

    UNSIGNED_LONG_LONG_MAX_RANGE = 18446744073709551615
    ARRAY_MAX_LEN = 5000000
    array1 = array.array('Q')

    v = random.randint(1, UNSIGNED_LONG_LONG_MAX_RANGE)
    for _ in range(ARRAY_MAX_LEN):
        while v in UNIQUE_SET:
            v = random.randint(1, UNSIGNED_LONG_LONG_MAX_RANGE)
        array1.append(v)
        UNIQUE_SET.add(v)

    test_values = [array1[random.randint(0, ARRAY_MAX_LEN)] for i in range(5)]

    test_values_not_exist = [0, -1, -5]

    print("\nARRAY('Q'), len: {}, memory: {}".format(len(array1), human_size(sys.getsizeof(array1))))
    
    print("\nSET, len: {}, memory: {}".format(len(array1), human_size(sys.getsizeof(UNIQUE_SET))))

    print("\nTest values {}".format(test_values))
    
    mergesort(array1)
    
    print("\nARRAY SORTED")

    for i in test_values:

        print(search0(array1, i))
        print(search1(UNIQUE_SET, i))


    for i in test_values_not_exist:

        print(search0(array1, i))
        print(search1(UNIQUE_SET, i))
