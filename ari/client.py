import collections
import json
import logging
import traceback
import urllib.parse

import httpx
import websockets

logger = logging.getLogger("ari")


class ARI:

    def __init__(self, baseaddr, auth=None, scheme="http"):
        self.AUTH = auth
        self.BASE_ADDR = baseaddr
        self.SCHEME = scheme
        self.BASE_URL = f"{{scheme}}://{self.BASE_ADDR}/ari/"
        self.WS_URL = f"ws://{self.BASE_ADDR}/ari/"
        self.events_cb = collections.defaultdict(list)

    def connect_cb(self, event_type, cb):
        self.events_cb[event_type].append(cb)

    def _make_url(self, path, path_params=None, query_params=None, ws=False):
        base = self.WS_URL if ws else self.BASE_URL.format(scheme=self.SCHEME)
        if path_params:
            path = path.format(**path_params)
        url = urllib.parse.urljoin(base, path)
        query_params = query_params if query_params else {}
        if self.AUTH:
            query_params.update({"api_key": f"{self.AUTH[0]}:{self.AUTH[1]}"})
        if query_params:
            params = urllib.parse.urlencode(query_params)
            url = f"{url}?{params}"
        return url

    async def ping(self):
        async with httpx.AsyncClient() as c:
            r = await c.get(self._make_url('asterisk/ping'))
            r.raise_for_status()
            try:
                answer = r.json()
                return True if answer.get("ping") == "pong" else False
            except Exception:
                logger.exception("ARI ping: {}".format(traceback.format_exc()))
                return False

    async def info(self):
        async with httpx.AsyncClient() as c:
            r = await c.get(self._make_url('asterisk/info'))
            r.raise_for_status()
            return r.json()

    async def modules(self):
        async with httpx.AsyncClient() as c:
            r = await c.get(self._make_url('asterisk/modules'))
            r.raise_for_status()
            return r.json()

    async def variable(self, name):
        async with httpx.AsyncClient() as c:
            r = await c.get(self._make_url('asterisk/variable', query_params={'variable': name}))
            r.raise_for_status()
            return r.json()

    async def channel_play_sound(self, channel_id, media, lang=None, offsetms=None, skipms=None,
                                 playbackId=None):
        optional = ("lang", "offsetms", "skipms", "playbackId")
        url = self._make_url(
            "channels/{channel_id}/play",
            path_params={"channel_id": channel_id},
            query_params={
                "media": media,
                **{k: v for k, v in locals().items() if k in optional and v is not None}
            }
        )
        async with httpx.AsyncClient() as c:
            r = await c.post(url)
            r.raise_for_status()
            return r.json()

    async def channel_continue(self, channel_id):
        url = self._make_url(
            "channels/{channel_id}/continue",
            path_params={"channel_id": channel_id},
        )
        async with httpx.AsyncClient() as c:
            r = await c.post(url)
            r.raise_for_status()
            return r.json()

    async def test_events_listen(self, app):
        url = self._make_url("events", query_params={"app": app}, ws=True)
        async with websockets.connect(uri=url) as ws:
            logger.info("Start ARI %s events listening", app)
            async for raw_msg in ws:
                data = json.loads(raw_msg)
                try:
                    logger.info(data)
                    [await i(data) for i in self.events_cb.get("*", [])]
                    if data['type'] == "StasisStart":
                        channel_id = data['channel']['id']
                        logger.info(await self.channel_continue(channel_id))
                        logger.info("Continue sent")
                        logger.info(await self.channel_play_sound(channel_id, "sound:hello"))
                        logger.info("Hello sent")
                except Exception:
                    logger.exception("Listen event: {}".format(traceback.format_exc()))
