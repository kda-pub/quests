import random
import unittest

from sorting import bubble_sort, quick_sort, select_sort


class TestSortMethods(unittest.TestCase):

  def test_quick_sort(self):
      for i in range(100):
          test_array = [int(100*random.random()) for i in range(1000)]
          test1_array = test_array
          self.assertEqual(quick_sort(test_array), sorted(test1_array))

  def test_bubble_sort(self):
      for i in range(100):
          test_array = [int(100*random.random()) for i in range(1000)]
          test1_array = test_array
          self.assertEqual(bubble_sort(test_array), sorted(test1_array))

  def test_select_sort(self):
      for i in range(100):
          test_array = [int(100*random.random()) for i in range(1000)]
          test1_array = test_array
          self.assertEqual(select_sort(test_array), sorted(test1_array))


if __name__ == '__main__':
    unittest.main(verbosity=2)
