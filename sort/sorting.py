import dataclasses
import json


@dataclasses.dataclass
class Stat:
    passed: int
    swaps: int
    checkings: int


def jprint(data):
    if isinstance(data, ("".__class__, u"".__class__)):
        data = json.loads(data)
    return json.dumps(dataclasses.asdict(data), indent=4, sort_keys=True)


def bubble_sort(alist, debug_stat=False):
    if debug_stat:
        stat = Stat(passed=0, swaps=0, checkings=0)
    if len(alist) <= 1:
        return (alist, stat)
    for i in range(len(alist)):
        for j in range(i+1, len(alist)):
            if debug_stat:
                stat.checkings += 1
            if alist[i] > alist[j]:
                alist[i], alist[j] = alist[j], alist[i]
                if debug_stat:
                    stat.swaps += 1
        if debug_stat:
            stat.passed += 1
    return (alist, stat) if debug_stat else alist


def select_sort(alist, debug_stat=False):
    if debug_stat:
        stat = Stat(passed=0, swaps=0, checkings=0)
    if len(alist) <= 1:
        return (alist, stat)
    for i in range(len(alist)):
        cmin = i
        for j in range(i+1, len(alist)):
            if debug_stat:
                stat.checkings += 1
            if alist[j] < alist[cmin]:
                cmin = j
        if debug_stat:
            stat.swaps += 1
            stat.passed += 1
        alist[i], alist[cmin] = alist[cmin], alist[i]
    return (alist, stat) if debug_stat else alist


def quick_sort(alist):
    if len(alist) <= 1:
        return alist
    first_list = []
    middle_list = []
    end_list = []
    val = alist[0]
    for n in alist:
       if n < val:
           first_list.append(n)
       elif n > val:
           end_list.append(n)
       else:
           middle_list.append(n)
    return quick_sort(first_list) + middle_list + quick_sort(end_list)


if __name__ == "__main__":
    print('{:*^80}'.format('bubble_sort'))
    result, stat = bubble_sort([100, 30, 40, 90, 50, 60, 70, 80, 20, 110], debug_stat=True)
    print("{}\n\nSTAT: {}".format(result, jprint(stat)))

    print('{:*^80}'.format('select_sort'))
    result, stat = select_sort([100, 30, 40, 90, 50, 60, 70, 80, 20, 110], debug_stat=True)
    print("{}\n\nSTAT: {}".format(result, jprint(stat)))

    print('{:*^80}'.format('quick_sort'))
    result = quick_sort([100, 30, 40, 90, 50, 60, 70, 80, 20, 110])
    print("{}\n\n".format(repr(result)))
